
import Philosopher._
import Table.{IAmHungry, PutDown}
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import java.util.Calendar

object PhilosopherState extends Enumeration {
  type State = Value
  val Thinking, Hungry, WaitingForLeftFork, WaitingForRightFork, Eating, Denied = Value
}

/**
  * Philosopher is created to eat or think
  * @param id: Int
  * @param table: ActorRef to Table
  * @param state: PhilosopherState.Value - Enum
  * @param eatingCounter: Int - current required eat times
  */
class Philosopher(val id: Int, val table: ActorRef, var state: PhilosopherState.Value, var eatingCounter: Int = 0) extends Actor with ActorLogging {
  def receive: Receive = {
    case Eat =>
      eatingCounter += 1
      if (state == PhilosopherState.Thinking) {
        state = PhilosopherState.Hungry
        table ! IAmHungry(id)
      }
    case LeftForkBusy =>
      state match {
        case PhilosopherState.WaitingForLeftFork =>
          table ! PutDown(id + 1) // the id of right fork
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
        case PhilosopherState.Hungry =>
          state = PhilosopherState.Denied
        case PhilosopherState.Denied =>
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
      }
    case RightForkBusy =>
      state match {
        case PhilosopherState.WaitingForRightFork =>
          table ! PutDown(id) // the id of left fork
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
        case PhilosopherState.Hungry =>
          state = PhilosopherState.Denied
        case PhilosopherState.Denied =>
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
      }
    case LeftForkFree =>
      state match {
        case PhilosopherState.Hungry =>
          state = PhilosopherState.WaitingForRightFork
        case PhilosopherState.WaitingForLeftFork =>
          state = PhilosopherState.Eating
          val now = Calendar.getInstance().getTime()
          log.info("Philosopher " + id + " is eating")
          eatingCounter -= 1
          Thread.sleep(1000)
          table ! PutDown(id)
          table ! PutDown(id +1)
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
        case PhilosopherState.Denied =>
          table ! PutDown(id)
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
      }
    case RightForkFree =>
      state match {
        case PhilosopherState.Hungry =>
          state = PhilosopherState.WaitingForLeftFork
        case PhilosopherState.WaitingForRightFork =>
          state = PhilosopherState.Eating
          log.info("Philosopher " + id + " is eating")
          eatingCounter -= 1
          Thread.sleep(1000)
          table ! PutDown(id)
          table ! PutDown(id +1)
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
        case PhilosopherState.Denied =>
          table ! PutDown(id + 1)
          if (eatingCounter > 0) {
            state = PhilosopherState.Hungry
            table ! IAmHungry(id)
          }
          else {
            state = PhilosopherState.Thinking
          }
      }

  }
}


object Philosopher {

  import PhilosopherState._

  def props(id: Int, table: ActorRef, state: PhilosopherState.Value): Props = {
    Props(new Philosopher(id, table, Thinking))
  }

  case object LeftForkFree

  case object RightForkFree

  case object LeftForkBusy

  case object RightForkBusy

  case object Eat

}

