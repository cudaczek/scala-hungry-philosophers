import Table.{AddPhilosopher, KillAllPlease, MakePhilosopherHungry}
import akka.actor.{ActorRef, ActorSystem, PoisonPill}

object Appl {

  def main(args: Array[String]) {

    val system: ActorSystem = ActorSystem("helloAkka")
    var number_of_philosophers: Int = 5
    var eating_times: Int = 5

    if (args.size == 1 ){
      number_of_philosophers = args(0).toInt
    }
    if (args.size == 2){
      number_of_philosophers = args(0).toInt
      eating_times = args(1).toInt
    }

    var table:  ActorRef =system.actorOf(Table.props(new Array[ActorRef](0), new Array[ActorRef](0)), "table")
    for (i <- 0 until number_of_philosophers) {
      val arystoteles: ActorRef = system.actorOf(Philosopher.props(i, table, PhilosopherState.Thinking ), ("Arystoteles" + i))
      val fork: ActorRef = system.actorOf(Fork.props(i, table ), ("fork" + i))
      table ! AddPhilosopher(arystoteles, fork)
    }
    for(j <- 0 until eating_times) {
      for (i <- 0 until  number_of_philosophers) {
        table ! MakePhilosopherHungry(i)
      }
      Thread.sleep(500)
    }

    Thread.sleep(30000)
    for (i <- 0 until number_of_philosophers) {
      table ! KillAllPlease
    }
    table ! PoisonPill

    system.terminate()
    println("KONIEC!")


  }


}