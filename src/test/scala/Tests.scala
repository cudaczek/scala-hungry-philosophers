import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{EventFilter, ImplicitSender, TestActorRef, TestActors, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, MustMatchers, WordSpecLike}

import scala.concurrent.duration._
import Table._
import Fork._
import Philosopher._

class Tests(_system: ActorSystem)
  extends TestKit(_system)
    with Matchers
    with WordSpecLike
    with BeforeAndAfterAll {

  def this() = this(ActorSystem("helloAkka"))

  override def afterAll: Unit = {
    shutdown(system)
  }


  "A Fork" should {
    "not be busy when ask the first time" in {
      val table = TestProbe()
      val fork = system.actorOf(Fork.props(0, table.ref))

      fork ! AreYouBusy(0)
      table.expectMsg(500 millis, IAmFree(0, 0))
    }

    "not be busy when ask the first time and be busy when ask the second time" in {
      val table = TestProbe()
      val fork = system.actorOf(Fork.props(0, table.ref))

      fork ! AreYouBusy(0)
      table.expectMsg(500 millis, IAmFree(0, 0))
      fork ! AreYouBusy(0)
      table.expectMsg(500 millis, IAmBusy(0, 0))
    }
  }

  "A Philosopher" should {
    "be hungry" in {
      val table = TestProbe()
      val platon = system.actorOf(Philosopher.props(0, table.ref, PhilosopherState.Thinking))

      platon ! Eat
      table.expectMsg(500 millis, IAmHungry(0))
    }

  }

}


